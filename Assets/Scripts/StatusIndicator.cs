﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusIndicator : MonoBehaviour
{
    [SerializeField]// so they show up in thw editor
    private RectTransform hpRec;
    [SerializeField]
    private Text hpText;

    private void Start()
    {
        if (hpRec == null)
        {
            Debug.LogError("No HP object!");
        }
        if (hpText == null)
        {
            Debug.LogError("No HP text object!");
        }
    }

    public void Health(int current, int max)
    {
        float value = (float)current / max;

        hpRec.localScale = new Vector3(value, hpRec.localScale.y, hpRec.localScale.z);//changes scale of health bar
        hpText.text = current + "/" + max + "HP";
        Image image = hpRec.GetComponent<Image>();
        if (current <= 0.25 * max)
            image.color = new Color(255, 0, 0);  //Health Bar colour = red if health = 25% of max
        else if (current <= 0.5 * max)
            image.color = new Color(229, 207, 0); //Health Bar colour = yellow if health = 50% of max
    }
}
