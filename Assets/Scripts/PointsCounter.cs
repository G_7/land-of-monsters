﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class PointsCounter : MonoBehaviour
{
    private Text points; 

    void Awake()
    {
        points = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        points.text = "Points: " + GameMaster.points.ToString();
    }
}
