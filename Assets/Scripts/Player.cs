﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UIElements.DropdownMenuAction;


[RequireComponent(typeof(PlayerMovment))]
public class Player : MonoBehaviour
{
   
    
    [SerializeField]
    private StatusIndicator statusIndicator;
    private PlayerStats playerStats;
   

    void Start()
    {
        //MaxHealth = 100;

        playerStats = PlayerStats.ps;
        //playerStats.maxHealth = playerStats.CurrentHealth;
        Debug.Log("YOU LOST EVERYTHING!");

        PlayerStats.ps.maxHealth = 100;
        PlayerStats.ps.speed = 10;
        PlayerStats.ps.damage = 10;
        if (statusIndicator == null)
        {
            //statusIndicator.Health(playerStats.CurrentHealth, playerStats.maxHealth);
            Debug.Log("No status indicator on player");
        }
        else
        {
            statusIndicator.Health(playerStats.CurrentHealth, playerStats.maxHealth);
        }
        GameMaster.GameM.onToggleUpgrade += Upgrades;

        InvokeRepeating("RegenerateHP", 1f/playerStats.regenRate, 1f/playerStats.regenRate);
            

        
    }

    void RegenerateHP()
    {
        playerStats.CurrentHealth += 1;
        statusIndicator.Health(playerStats.CurrentHealth, playerStats.maxHealth);
    }

    void Update()
    {

        if (transform.position.y <= playerStats.fall)
        {
            DamageHealthP(999999);
            SoundManager.PlayAudio("Hurt");
        }
    }
    void Upgrades(bool active) //when upgrade menu is toggled
    {
        GetComponent<PlayerMovment>().enabled = !active;//disable movement
        Weapon weapon = GetComponentInChildren<Weapon>();
        if (weapon != null)
        {
            weapon.enabled = !active; 
        }
    }

    //gameM.onToggleUpgrade += Upgrades;
    
    void OnDestroy()
    {
        GameMaster.GameM.onToggleUpgrade -= Upgrades;
    }

    public void DamageHealthP(int amount)
    {
        playerStats.CurrentHealth -= amount;
        SoundManager.PlayAudio("Hurt");
        if (playerStats.CurrentHealth <= 0)
        {
            //Debug.Log("YOU DIED");
            GameMaster.ByeByePlayer(this);
            


        }
        statusIndicator.Health(playerStats.CurrentHealth, playerStats.maxHealth);
    }
    
}
