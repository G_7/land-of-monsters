﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTail : MonoBehaviour
{
    public int moveSpeed = 230; 

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * moveSpeed); //move something with the passage of time. Takes direction and spacial orientation
        Destroy(gameObject, 1);
    }
}
