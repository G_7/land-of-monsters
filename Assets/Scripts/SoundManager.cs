﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip walk, jump, shoot, hurt, death, click, money, noMoney, enemyDeath, respawning, level1, level2, level3;
    static AudioSource audioScr;

    // Start is called before the first frame update
    void Start()
    {
        walk = Resources.Load<AudioClip>("Walk");
        jump = Resources.Load<AudioClip>("Jump");
        shoot = Resources.Load<AudioClip>("Shoot");
        hurt = Resources.Load<AudioClip>("Hurt");
        death = Resources.Load<AudioClip>("GameOverPlayerDeath");
        click = Resources.Load<AudioClip>("ButtonClick");
        money = Resources.Load<AudioClip>("Spending");
        noMoney = Resources.Load<AudioClip>("NotEnoughMoney");
        respawning = Resources.Load<AudioClip>("Respawning");
        enemyDeath = Resources.Load<AudioClip>("EnemyDeath");
        //level1 = Resources.Load<AudioClip>("MusicLevel1");
        //level2 = Resources.Load<AudioClip>("MusicLevel2");
        //level3 = Resources.Load<AudioClip>("MusicLevel3");
        //
        audioScr = GetComponent<AudioSource>();
        //audioScr.volume = Random.Range(0.3f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlayAudio(string clip)
    {
        switch (clip)
        {
            case "Walk":
                audioScr.PlayOneShot(walk);
                break;

            case "Jump":
                audioScr.PlayOneShot(jump);
                break;

            case "Shoot":
                audioScr.PlayOneShot(shoot);
                break;

            case "Hurt":
                audioScr.PlayOneShot(hurt);
                break;

            case "GameOverPlayerDeath":
                audioScr.PlayOneShot(death);
                break;

            case "ButtonClick":
                audioScr.PlayOneShot(click);
                break;

            case "Spending":
                audioScr.PlayOneShot(money);
                break;

            case "NotEnoughMoney":
                audioScr.PlayOneShot(noMoney);
                break;

            case "Respawning":
                audioScr.PlayOneShot(respawning);
                break;

            case "EnemyDeath":
                audioScr.PlayOneShot(enemyDeath);
                break;

          //case "MusicLevel1":
          //    audioScr.PlayOneShot(level1);
          //    
          //    break;
          //
          //case "MusicLevel2":
          //    audioScr.PlayOneShot(level2);
          //    break;
          //
          //case "MusicLevel3":
          //    audioScr.PlayOneShot(level3);
          //    break;


        }
    }
}
