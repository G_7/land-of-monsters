﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
    public void Quit()
    {
        SoundManager.PlayAudio("ButtonClick");
        Debug.Log("QUIT");
        Application.Quit();

    }

    public void Restart()
    {
        //Application.LoadLevel(Application.loadedLevel);
        SoundManager.PlayAudio("ButtonClick");
        SceneManager.LoadScene("Level1");
        GameMaster.LivesLeftPlayer = 3; // is static variable so its isnt automatically reset
    }

    public void Play()
    {
        SoundManager.PlayAudio("ButtonClick");
        SceneManager.LoadScene("Level1");
       
    }

    public void Settings()
    {
        SoundManager.PlayAudio("ButtonClick");
        
        SceneManager.LoadScene("Settings");
    }
    public void StartMenu()
    {
        SoundManager.PlayAudio("ButtonClick");
        SceneManager.LoadScene("StartMenu");

    }
}
