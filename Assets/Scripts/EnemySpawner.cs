﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EnemySpawner : MonoBehaviour
{
    [System.Serializable]
    public class Wave
    {
        public string waveLevel;
        public Transform enemy;
        public Transform enemy1;
        public Transform enemy2;
        public int amount;
        public float spawnRate;
    }
    public Wave[] waves;
    public Transform[] spawnPoints;
    
    public GameObject portal;
    public GameObject Portal
    {
        get { return portal; }
        
    }
    [SerializeField]
    private int pointsToNext;

    private int nextWave = 0;
    public int NextWave
    {
        get { return nextWave + 1; }
    }
    public float timeBetween = 5f;
    public float countdown = 0f;
    public float Countdown
    {
        get { return countdown; }
    }

    private float searchTime = 1f;
    private spawnState state = spawnState.counting;

    public spawnState State
    {
        get { return state; }
    }

    public enum spawnState
    {
        spawning,
        waiting,
        counting
 
    };

    void Start()
    {
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No spawn points");
        }
        countdown = timeBetween;
        
    }

    
    void Update()
    {
        KillAll();
        if (state == spawnState.waiting)
        {
            if (!IsEnemyAlive())
            {
                Complete();
                return;
            }
            else
            {
                return;
            }
        }

        if (countdown <= 0) //if started to spawn waves
        {
            if (state != spawnState.spawning)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
            
        }
        else
        {
            countdown -= Time.deltaTime; //go down the right amount of time for each frame
            //not 0 yet so counts doen from 5
        }

        
    }
    bool IsEnemyAlive()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0f)
        {
            countdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                return false;
            }
        }
        return true;
    }
    void Complete()
    {
        Debug.Log("Wave Over");
        state = spawnState.counting;
        countdown = timeBetween;
        
        if (nextWave + 1 > waves.Length - 1)
        {
            if (GameMaster.collectionPoints >= pointsToNext)
            {
                state = spawnState.waiting;
            }
            else
                nextWave = 0;
            Debug.Log("All Levels Complete");
            
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); 
            //Can start a new scene here for diff level

        }
       
        else
        {
            nextWave++;
            //portal.SetActive(false);
        }
        

    }

    IEnumerator SpawnWave(Wave wave)//so we can wait a curtain amount of secs in the methods
    {
        Debug.Log("Spawning wave" + wave.waveLevel);
        state = spawnState.spawning;
        //spawning
        for (int i = 0; i < wave.amount; i++)
        {
           
            if (wave.enemy != null)
            {
                SpawnEnemy(wave.enemy);
            }
            
            //yield return new WaitForSeconds(1 / wave.spawnRate);
            if (wave.enemy1 != null)
            {
                SpawnEnemy(wave.enemy1);
               
            }
            
            //yield return new WaitForSeconds(1 / wave.spawnRate);
            if (wave.enemy2 != null)
            {
                SpawnEnemy(wave.enemy2);

            }
            else
            {
                ;
            }
            
            yield return new WaitForSeconds(1/wave.spawnRate);
        }
        state = spawnState.waiting;
        yield break;
    }
    void SpawnEnemy(Transform enemy)
    {
        if (!Input.GetKeyDown(KeyCode.U))
        {
            Debug.Log("Spawning: " + enemy.name);

            Transform sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
            Instantiate(enemy, sp.position, sp.rotation);
        }
        else
        {
            return;
        }

        
    }
    public void KillAll()
    {
        if (GameMaster.collectionPoints >= pointsToNext)
        {
           
            Debug.Log("Show ME Now!" + GameMaster.collectionPoints);
            portal.SetActive(true);
            state = spawnState.waiting;
        }
        else
        {
            Debug.Log("Dont Show ME Now!");
            portal.SetActive(false);
        }
    }
    //public string Level;

    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    Debug.Log("NextLevel");
    //    if (other.gameObject.tag == "Player")
    //    {
    //       SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex + 1);
    //    }
    //}

}
