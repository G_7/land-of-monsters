﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public static bool isPaused;

    public GameObject pauseMenu;

   //public delegate void UpgradeUICall(bool active); //delagate creates a types that stores refs of functions
   //public UpgradeUICall onToggleUpgrade;
   //
   //public GameObject upgradesUI;

    // Update is called once per frame
    void Update()
    {
        if (isPaused)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;

        }
        else
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            isPaused = !isPaused;
        }
    }

    public void Resume()
    {
        SoundManager.PlayAudio("ButtonClick");
        isPaused = !isPaused;
    }
    public void Menu()
    {
        SoundManager.PlayAudio("ButtonClick");
        SceneManager.LoadScene("StartMenu");
    }
    public void Quit()
    {
        SoundManager.PlayAudio("ButtonClick");
        Debug.Log("QUIT");
        Application.Quit();

    }
}
//13.25, 7.96