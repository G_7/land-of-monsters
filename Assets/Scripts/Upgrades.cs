﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Upgrades : MonoBehaviour
{
    [SerializeField]
    public Text Health;
    [SerializeField]
    public Text Speed;
    [SerializeField]
    public Text Damage;

    [SerializeField]
    public float HealthMulti = 1.3f;

    [SerializeField]
    public float SpeedMulti = 1.3f;

    [SerializeField]
    public float DamageMulti = 1.2f;

    private PlayerStats playerStats;

    private StatusIndicator statusIndicator;


    private void Start()
    {
        //statusIndicator.Health(playerStats.CurrentHealth, playerStats.maxHealth);

    }
    private void OnEnable()
    {
        playerStats = PlayerStats.ps;
        StatVals();
    }

    void StatVals()
    {
        Health.text = "Health: " + playerStats.maxHealth.ToString();
        Speed.text = "Speed: " + playerStats.speed.ToString();
        Damage.text = "Damage: " + playerStats.damage.ToString();
    }

    public void UpgradeHealth()
    {
       
        if (GameMaster.points < 10)
        {
            SoundManager.PlayAudio("NotEnoughMoney");
            return;
            
        }
        SoundManager.PlayAudio("Spending");

        playerStats.maxHealth = (int)(playerStats.maxHealth * HealthMulti);
        playerStats.CurrentHealth = playerStats.maxHealth;
        GameMaster.points -= 10;
        StatVals();
        Debug.Log("Health!");

    }

    // Update is called once per frame
    public void UpgradeSpeed()
    {
       ///if (GameMaster.points >= 20)
       ///{
       ///   
       ///}
        
        if (GameMaster.points < 20)
        {
            SoundManager.PlayAudio("NotEnoughMoney");
            return;
        }
        SoundManager.PlayAudio("Spending");
        playerStats.speed = (int)(playerStats.speed * SpeedMulti);
        GameMaster.points -= 20;
        StatVals();
        Debug.Log("Speed!");
    }

    public void UpgradeDamage()
    {
       //if (GameMaster.points >= 30)
       //{
       //    
       //}
        if (GameMaster.points < 30)
        {
            SoundManager.PlayAudio("NotEnoughMoney");
            return;
            
        }
        SoundManager.PlayAudio("Spending");
        playerStats.damage = (int)(playerStats.damage * DamageMulti);
        GameMaster.points -= 30;
        StatVals();
        Debug.Log("Damage!");
    }
    
}
