﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class PortalScript : MonoBehaviour
{
    
    
   
    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("NextLevel");
        if (other.CompareTag("Player"))
        {
            GameMaster.livesLeftPlayer = 3;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            
        }
    }

}