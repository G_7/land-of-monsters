﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameMaster : MonoBehaviour
{
    public static GameMaster GameM;

    [SerializeField]
    private int startingPoints;
    public static int points;
    public static int collectionPoints;


    public static int livesLeftPlayer = 3;
    public GameObject heart1, heart2, heart3;
    public static int LivesLeftPlayer
    {
        get { return livesLeftPlayer; }
        set { livesLeftPlayer = value; }
    }

    public GameObject upgradesUI;

    public delegate void UpgradeUICall(bool active); //delagate creates a types that stores refs of functions
    public UpgradeUICall onToggleUpgrade;
    private int sPlay = 0;

    void Awake()
    {
        
        if (GameM == null)
        {
            GameM = GameObject.FindGameObjectWithTag("GameM").GetComponent<GameMaster>(); //ref to an instance of class


        }

       
    }
    public Transform playerPrefab;
    public Transform spawnPlace;
    public int SpawnTime = 2;
    public Transform spawnPrefab;

    //public Transform EnemyParticles;
    public CameraShake camShake;

    public EnemySpawner eS;

    private void Start()
    {
        if (camShake == null)
        {
            Debug.Log("No camera shake in gamemaster");
        }
        heart1.gameObject.SetActive(true);
        heart2.gameObject.SetActive(true);
        heart3.gameObject.SetActive(true);

        points = startingPoints;
        collectionPoints = 0;
        livesLeftPlayer = 3;





    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            ToggleUpgrade();
        }
      // if (sPlay == 0)
      // {
      //     SoundManager.PlayAudio("MusicLevel1");
      //     sPlay = 500;
      // }
      // else
      // {
      //     sPlay--;
      //
      // }
      // 
    }
    private void ToggleUpgrade()
    {
        upgradesUI.SetActive(!upgradesUI.activeSelf);
        eS.enabled = !upgradesUI.activeSelf;
        onToggleUpgrade.Invoke(upgradesUI.activeSelf);

    }
    public IEnumerator Respawn()
    {
        yield return new WaitForSeconds(SpawnTime);
        Instantiate(playerPrefab, spawnPlace.position, spawnPlace.rotation);
        Transform clone = Instantiate(spawnPrefab, spawnPlace.position, spawnPlace.rotation) as Transform;
        Destroy(clone.gameObject, 3f);
        
    }

    public void EndOfGame()
    {
        Debug.Log("GameOver");
        SceneManager.LoadScene("GameOver");
    }
    public void Hearts()
    {
        switch (livesLeftPlayer)
        {
            case 3:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                break;

            case 2:
                heart1.gameObject.SetActive(false);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                break;

            case 1:
                heart1.gameObject.SetActive(false);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(true);
                break;

            case 0:
                heart1.gameObject.SetActive(false);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                break;
        }
    }
    public static void ByeByePlayer(Player player) // static so don't have to reference it every time 
    {
        Destroy(player.gameObject); //destroys the player
        livesLeftPlayer --;
       
        GameM.Hearts();
        if (livesLeftPlayer <= 0)
        {
            //SoundManager.PlayAudio("GameOverPlayerDeath");
            GameM.EndOfGame();
            


        }
        else
        {
            GameM.StartCoroutine(GameM.Respawn());
            SoundManager.PlayAudio("Respawning");
            PlayerStats.ps.CurrentHealth = PlayerStats.ps.maxHealth;


        }
    }

    public static void ByeByeEnemy(Enemy enemy) // static so don't have to reference it every time 
    {
        GameM.KillEnemy(enemy);
        //destroys the Enemy
        //GameM.StartCoroutine(GameM.Respawn());
       
        
    }

    public void KillEnemy(Enemy enemy)
    {
        
        Transform EParticles =  Instantiate(enemy.EnemyParticles, enemy.transform.position, Quaternion.identity) as Transform;
        Destroy(EParticles.gameObject, 2f);
        camShake.shake(enemy.shakeAmount, enemy.shakeLength);
        Destroy(enemy.gameObject);
    }

    

}
