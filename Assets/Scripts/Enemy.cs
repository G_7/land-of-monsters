﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyAI))]
public class Enemy : MonoBehaviour
{
    [System.Serializable]
    public class EnemyStats
    {
        public int maxHealth;
        public int currentHealth;
        public int damage = 20;

        //public float PercentStart = 1f;

        public int CurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = Mathf.Clamp(value, 0, maxHealth); } //value to set it to any value
        }

        public void Init() //take care of variable that start at the begining of run time or compile time
        {
            CurrentHealth = maxHealth; //* PercentStart;
        }

        
    }
    public EnemyStats enemyStats = new EnemyStats(); // create an instance of the class

    public Transform EnemyParticles;
    public float shakeAmount = 0.1f;
    public float shakeLength = 0.1f;
    
    public int pointsOnKill;
    



    [Header("Optional: ")] //attribute allow youto type something into editor/ inspector
    [SerializeField]
    private StatusIndicator statusIndicator; //reference it

    private void Start()
    {
        if (statusIndicator != null)
        {
            statusIndicator.Health(enemyStats.CurrentHealth, enemyStats.maxHealth);
        }

        enemyStats.Init();

        if (EnemyParticles == null)
        {
            Debug.Log("No death Particles");
        }
        GameMaster.GameM.onToggleUpgrade += Upgrades;
    }
    void Upgrades(bool active) //when upgrade menu is toggled
    {
        GetComponent<EnemyAI>().enabled = !active;//disable movement
        
    }
    public void DamageHealthE(int amount )
    {
        enemyStats.CurrentHealth -= amount;
        if (enemyStats.CurrentHealth <= 0)
        {
            //Debug.Log("Enemy down!");
            //GameMaster.ByeByePlayer(this);
            GameMaster.ByeByeEnemy(this);
            //SoundManager.PlayAudio("EnemyDeath");
            GameMaster.points += pointsOnKill;
            GameMaster.collectionPoints += pointsOnKill;
        }
        if (statusIndicator != null)
        {
            statusIndicator.Health(enemyStats.CurrentHealth, enemyStats.maxHealth);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Player player = collision.collider.GetComponent<Player>();
        if (player != null)
        {
            player.DamageHealthP(enemyStats.damage);
            SoundManager.PlayAudio("Hurt");
            
            DamageHealthE(999);
            GameMaster.points -= pointsOnKill;
            GameMaster.collectionPoints -= pointsOnKill;

        }
    }

    void OnDestroy()
    {
        GameMaster.GameM.onToggleUpgrade -= Upgrades;
    }
}
