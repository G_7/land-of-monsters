﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;




public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    Resolution[] res;
    public Dropdown resDropdown;
    //public Slider volSlider;
    

    void Start()
    {
        res = Screen.resolutions;
        resDropdown.ClearOptions();
        List<string> options = new List<string>();
        int currentResIndex = 0;
        for (int i = 0; i < res.Length; i++)
        {
            string option = res[i].width + "X" + res[i].height;
            options.Add(option);
            if (res[i].width == Screen.currentResolution.width && res[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }
        resDropdown.AddOptions(options);
        resDropdown.value = currentResIndex;
        resDropdown.RefreshShownValue();

        
    }

    public void SetVol(float vol)
    {
        audioMixer.SetFloat("Volume", vol);
        Debug.Log("Sound");
        
    }
    
    public void SetGraphicsQual(int qualIndex)
    {
        QualitySettings.SetQualityLevel(qualIndex);
    }

    public void FullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetRes(int resIndex)
    {
       Resolution resolution = res[resIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
}
