﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public float fireRate = 0;
    //public int damage = 10;
    private PlayerStats playerStats;

   
    public LayerMask yesHit; // so you can choose what layers the lazer hits

    float whenFire = 0; //place ti time of next shot
    float spawnEffect = 0;
    public float spawnRate = 10;
    Transform firePoint;
    public Transform BulletTail;
    public Transform hitPrefab;
    public Transform muzzleFlash;

    // camera shake
    public float camShakeValue = 0.01f;
    public float shakeLength = 0.1f;
    CameraShake camShake;

    // Start is called before the first frame update
    void Awake()
    {
        
        firePoint = transform.Find("FireLine");
        if (firePoint == null)
        {
            Debug.LogError("No FirePoint!!!");
        }

        //Damage = 10;
    }
    private void Start()
    {
        playerStats = PlayerStats.ps;
        camShake = GameMaster.GameM.GetComponent<CameraShake>(); //GameM to get local instance and GetComponent to 
                                                                //get thre camera shake on that object
        if (camShake == null)
        {
            Debug.LogError("No camera shake script found");
        }
    }
    // Update is called once per frame
    void Update()
    {
        //Shoot();
        if (fireRate == 0)// one blast at time
        {
            if (Input.GetButtonDown("Fire1") && Pause.isPaused == false)
            {
                Shoot();
                SoundManager.PlayAudio("Shoot");
                
            }
        }
        else
        {
            if (Input.GetButton("Fire1") && Time.time > whenFire)
            {
                whenFire = Time.time + 1 / fireRate; //current time divided by fire rate
                Shoot();
            }
        }
    }

   //public void UpgradeWeaponDamage()
   //{
   //
   //    Damage = 20;
   //    Debug.Log("Damage!");
   //}

    void Shoot()
    {
        //Debug.Log("Test");
        Vector2 mousePos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y); //get pos of mouse from screen pos to world pos
        Vector2 firePos = new Vector2(firePoint.position.x, firePoint.position.y);
        RaycastHit2D hit = Physics2D.Raycast(firePos, (mousePos - firePos), 100, yesHit); //origin, direction, how far, what to hit
       
        
        Debug.DrawLine(firePos, (mousePos - firePos) * 100, Color.cyan);
        if (hit.collider != null)
        {
            //Debug.Log("hi");
            Debug.DrawLine(firePos, hit.point, Color.red);
            Enemy enemy = hit.collider.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.DamageHealthE(playerStats.damage);
            }
        }
        if (Time.time >= spawnEffect)
        {
            Vector3 hitPos;
            Vector3 hitNorm;

            if (hit.collider == null)
            {
                hitPos = (mousePos - firePos) * 30;
                hitNorm = new Vector3(9999,9999,9999);
            }
            else
            {
                hitPos = hit.point; //returns vector3 position the raycast hits
                hitNorm = hit.normal;
            }
            Effect(hitPos, hitNorm);
            spawnEffect = Time.time + 1 / spawnRate;
        }
    }

    void Effect( Vector3 hitPos, Vector3 hitNorm)
    {
        Transform trail = Instantiate(BulletTail, firePoint.position, firePoint.rotation) as Transform; // creating instance of this object
        LineRenderer lr = trail.transform.GetComponent<LineRenderer>();

        if (lr != null)
        {
            lr.SetPosition(0,firePoint.position);
            lr.SetPosition(1, hitPos);
        }
        Destroy(trail.gameObject, 0.02f);

        if (hitNorm != new Vector3(9999,9999,9999))
        {
            Debug.Log("No Effect!!!!!!!!!!");
           Transform hitPart = Instantiate(hitPrefab, hitPos, /*firePoint.rotation);*/ Quaternion.FromToRotation(Vector3.forward, hitNorm)) as Transform;
            Destroy(hitPart.gameObject, 1f);
        }

        Transform muzzleFlashClone = Instantiate(muzzleFlash, firePoint.position, firePoint.rotation) as Transform; // so only this instance is effected
        muzzleFlashClone.parent = firePoint;
        float size = Random.Range(0.6f, 0.9f);
        
        muzzleFlashClone.localScale = new Vector3(size, size, size);
        
        Destroy(muzzleFlashClone.gameObject, 0.02f);

        camShake.shake(camShakeValue, shakeLength); // make the camera shake

    }
}
