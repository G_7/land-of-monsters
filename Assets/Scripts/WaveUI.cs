﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WaveUI : MonoBehaviour
{
    [SerializeField]
    EnemySpawner spawner;

    [SerializeField]
    Animator waveAnim;

    [SerializeField]
    Text countdownText;

    [SerializeField]
    Text coutText;

    private EnemySpawner.spawnState previousState;

    // Start is called before the first frame update
    void Start()
    {
        if (spawner == null)
        {
            Debug.LogError("No spawner");
            this.enabled = false;
        }

        if (waveAnim == null)
        {
            Debug.LogError("No waveAnim");
            this.enabled = false;

        }

        if (countdownText == null)
        {
            Debug.LogError("No countdownText");
            this.enabled = false;

        }

        if (coutText == null)
        {
            Debug.LogError("No coutText");
            this.enabled = false;

        }

    }

    // Update is called once per frame
    void Update()
    {
        switch(spawner.State)
        {
            case EnemySpawner.spawnState.counting:
                UpdateCountDownUI();
                break;
            case EnemySpawner.spawnState.spawning:
                UpdateIncomingUI();
                break;

           //case EnemySpawner.spawnState.waiting:
           //    UpdateWaitingUI();
           //    break;
        }
        previousState = spawner.State;
    }
    void UpdateCountDownUI()
    {
        if (previousState != EnemySpawner.spawnState.counting)
        {
            waveAnim.SetBool("waveIncoming", false);
            waveAnim.SetBool("waveCountdown", true);
            Debug.Log("Counting");
        }
        countdownText.text = ((int)spawner.Countdown).ToString();
        
    }

    void UpdateIncomingUI()
    {
        if (previousState != EnemySpawner.spawnState.spawning)
        {
            waveAnim.SetBool("waveCountdown", false);
            waveAnim.SetBool("waveIncoming", true);
            Debug.Log("Spawning");
        }
        coutText.text = spawner.NextWave.ToString();
    }
  //void UpdateWaitingUI()
  //{
  //    Debug.Log("Waiting");
  //}
}
