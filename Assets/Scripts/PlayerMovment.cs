﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovment : MonoBehaviour
{
    
    //private float speed = 10f; //private so you can access from other scripts
    private PlayerStats playerStats;

    private Rigidbody2D rb2d; //contains rigidbody

    private Animator anim;

    private bool facingRight;

    //[SerializeField]
    private Transform groundPoint;

    private bool grounded;

    [SerializeField]
    private float groundedRadius;
    [SerializeField]
    private LayerMask whatIsGround;

    private bool jumping;

    [SerializeField]
    private float jumpForce;

   [SerializeField]
   private bool airControl;

   Transform playerGraphics; // ref to graphics so can change direction

    private int sPlay = 0;

    // Start is called before the first frame update
    private void Start()
    {
        //Speed = 10;
        playerStats = PlayerStats.ps;
    }

    void Awake()
    {
        facingRight = true; //always facing right in the start
        rb2d = GetComponent<Rigidbody2D>(); //make ref between this and the rigidbody on player
        anim = GetComponent<Animator>();
        groundPoint = transform.Find("GroundCheck");

        playerGraphics = transform.Find("Graphics");
        if (playerGraphics == null)
        {
            Debug.LogError("Were did the griphics go? ");
        }
        
    }


    private void Update()
    {
        HandleInput();

    }

    // Update is called once per frame
    void FixedUpdate() //upadtes a fixed amout of times based on time step
    {
        grounded = false;
        
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundPoint.position, groundedRadius, whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
                grounded = true;
        }
        anim.SetBool("Ground", grounded);
        float horizontal = Input.GetAxis("Horizontal");
        //grounded = IsGrounded();
        Movement(horizontal);
        //Flip(horizontal);
        
        ResetValuse();
    }

    private void Movement(float horizontal)
    {
        //Input.GetAxis("Horizontal") is getting the x axis (horizontal). 
        //This only allows movement left/ right. can use arrow or A/D keys
        rb2d.velocity = new Vector2(horizontal * playerStats.speed, rb2d.velocity.y); //velocity is a 2d vector. 


        if (grounded || airControl)
        {
            rb2d.velocity = new Vector2(horizontal * playerStats.speed, rb2d.velocity.y);
            anim.SetFloat("Speed", Mathf.Abs(horizontal));

            // If the input is moving the player right and the player is facing left...
            if (horizontal > 0 && !facingRight)
            {
                // ... flip the player.
                Flip(horizontal);
            }
            // Otherwise if the input is moving the player left and the player is facing right...
            else if (horizontal < 0 && facingRight)
            {
                // ... flip the player.
                Flip(horizontal);
            }
        }
        if (grounded && jumping == true && anim.GetBool("Ground"))
        {
            grounded = false;
            anim.SetBool("Ground", false);
            rb2d.AddForce(new Vector2(0f, jumpForce));
            SoundManager.PlayAudio("Jump");

        }

        if ((horizontal > 0 || horizontal < 0) && grounded)
        {
            if (sPlay == 0)
            {
                SoundManager.PlayAudio("Walk");
                sPlay = 15;
            }
            else
            {
                sPlay--;

            }

        }
        //if (grounded == true && jumping == false)
        //{
        //    Debug.Log("Animation?");
        //}
        //if (grounded == false && jumping == true)
        //{
        //    Debug.Log("Jumping");
        //    
        //}
        
        
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("SlideingPlatform "))
        {
            this.transform.parent = collision.transform;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("SlideingPlatform "))
        {
            this.transform.parent = null;
        }
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumping = true;
           

        }
    }

    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight; //false if its true or true if its false
            Vector3 Scale = playerGraphics.localScale;// ref to players local scale
            Scale.x *= -1;
            playerGraphics.localScale = Scale; //adding to players scale
        }
    }

    

   private void ResetValuse()
   {
       jumping = false;
       
   }
}
