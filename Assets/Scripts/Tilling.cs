﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]

public class Tilling : MonoBehaviour
{
    public int offsetX = 2;

    public bool rightExtra = false;
    public bool LeftExtra = false;
    public bool reverseScale = false;
    private float spriteWidth = 0f;
    private Camera cam;
    private Transform transformation;


    private void Awake()
    {
        cam = Camera.main;
        transformation = transform;
    }


    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer sRender = GetComponent<SpriteRenderer>();
        spriteWidth = sRender.sprite.bounds.size.x;

    }

    // Update is called once per frame
    void Update()
    {
        if (rightExtra == false || LeftExtra == false)
        {
            float camY = cam.orthographicSize * Screen.width / Screen.height;
            //equals length from center of camera to its right side.
            float visibleRight = (transformation.position.x + spriteWidth / 2) - camY;
            //where they intersect
            float visibleLeft = (transformation.position.x - spriteWidth / 2) + camY; //left edge of sprite is in view

            if (cam.transform.position.x >= visibleRight - offsetX && rightExtra == false)//right edge of sprite is in view and if instanciated
            {
                CreateExtention(1);
                rightExtra = true;
            }
            else if (cam.transform.position.x <= visibleLeft + offsetX && LeftExtra == false)//left edge of sprite is in view and if instanciated
            {
                CreateExtention(-1);
                LeftExtra = true;
            }
        }

    }

    void CreateExtention(int direction)
    {
        Vector3 newPos = new Vector3(transformation.position.x + spriteWidth * direction, transformation.position.y, transformation.position.z);
        Transform newSprite = Instantiate(transformation, newPos, transformation.rotation) as Transform;

        if (reverseScale == true)
        {
            //reverses the x value of sprie to hide seem
            newSprite.localScale = new Vector3(newSprite.localScale.x * -1, newSprite.localScale.y, newSprite.localScale.z);
            newSprite.parent = transformation.parent; //parents every new sprite to the first one

            if (direction > 0)
            {
                newSprite.GetComponent<Tilling>().LeftExtra = true;
            }
            else
            {
                newSprite.GetComponent<Tilling>().rightExtra = true;
            }

        }
       
    }
}
