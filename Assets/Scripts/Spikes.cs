﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    private PlayerStats playerStats;

    private void Start()
    {
        playerStats = PlayerStats.ps;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("NextLevel");
        if (other.CompareTag("Player"))
        {
            playerStats.CurrentHealth -= 30;
        }
    }
}
