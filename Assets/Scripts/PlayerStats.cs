﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats: MonoBehaviour
{
    public static PlayerStats ps;

    public int fall = -20;
    public int maxHealth = 100;
    public int speed = 10;
    public int damage = 10;

    public float regenRate = 2f;

    private int currentHealth = 100;
    //public float PercentStart = 1f;

    public int CurrentHealth
    {
        get { return currentHealth; }
        set { currentHealth = Mathf.Clamp(value, 0, maxHealth); } //value to set it to any value
    }

    void Awake() 
    {
        if (ps == null)
        {
            ps = this;
        }

        CurrentHealth = maxHealth; //* PercentStart;
        
    }

}
