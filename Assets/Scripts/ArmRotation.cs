﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmRotation : MonoBehaviour
{

    public int rotOffset = 90;
    // Update is called once per frame
    void Update()
    {
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position; //subtraction player position from mouse pos
        diff.Normalize(); // makes sum of vector equal to 1

        float rotationz = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;// finding angle in degrees
        transform.rotation = Quaternion.Euler(0f, 0f, rotationz + rotOffset);
    }
}
