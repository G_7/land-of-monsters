﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallaxing : MonoBehaviour
{
    //variables
    public Transform[] backgrounds;
    private float[] scales;
    public float smooth = 1f;

    private Transform cam;
    private Vector3 previousCamPos;

    private void Awake() // called before start but after game obj are setup.
    {
        cam = Camera.main.transform;
    }


    // Start is called before the first frame update
    void Start()
    {
        previousCamPos = cam.position; //previous frame has cams current position

        scales = new float[backgrounds.Length];

        for (int i = 0; i < backgrounds.Length; i++) // asigning scales
        {
            scales[i] = backgrounds[i].position.z - 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < backgrounds.Length; i++) 
        {
            float parallax = (previousCamPos.x - cam.position.x) * scales[i];
            //diff between cams position now and before times amount of parallaxing
            float backgroundX = backgrounds[i].position.x + parallax;

            Vector3 backgroundTargetPos = new Vector3(backgroundX, backgrounds[i].position.y, backgrounds[i].position.z);
            //assign background target pos on x and keep y and z

            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smooth * Time.deltaTime);
            //fades between current pos and target pos

        }
        previousCamPos = cam.position;
        //previous cam pos to cam pos at end of frame

    }
}
