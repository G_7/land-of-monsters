﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public Camera cam;
    float amountOfShake = 0;

    private void Awake()
    {
        if (cam == null)
        {
            cam = Camera.main;
        }
    }
   // void Update()
   // {
   //     if (Input.GetKeyDown(KeyCode.T))
   //     {
   //         shake(0.1f, 0.2f);
   //     }   
   // }

    public void shake(float amount, float length)
    {
        amountOfShake = amount;
        InvokeRepeating("StartShaking", 0, 0.01f); //call the startshaking methond, imediately every 0.01 secs
        Invoke("EndShaking", length);
       
    }

    void StartShaking()
    {
        if (amountOfShake > 0)
        {
            Vector3 camPos = Camera.main.transform.position; //position of main camera in temp value

            float shakingByX = Random.value * amountOfShake * 2 - amountOfShake; //how we want to offset camera
            float shakingByY = Random.value * amountOfShake * 2 - amountOfShake;
            //float shakingByZ = Random.value * amountOfShake * 2 - amountOfShake; // orthogonal camera so dont need it on z axis

            camPos.x += shakingByX; //add offset amount to the axis
            camPos.y += shakingByY;

            cam.transform.position = camPos; // put to temp value of cams pos into the main cameras transform

        }
    }

    void EndShaking()
    {
        CancelInvoke("StartShaking");
        cam.transform.localPosition = Vector3.zero; //to reset
    }

}
